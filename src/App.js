import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { Navbar } from "react-bootstrap";
import Button from 'react-bootstrap/Button'
import Nav from 'react-bootstrap/Nav'
import 'bootstrap/dist/css/bootstrap.min.css';
import Routes from "./Routes";
import "./App.css";

function App(props) {

  const [isAuthenticated, userHasAuthenticated] = useState("");
  const [tipo_usuario, setTipoUsuario] = useState("");
  const [paginaApp, setPaginaApp] = useState("");

  useEffect(() => {
    async function onLoad() {
      fetch(`http://localhost:8082/session/`, {
        method: 'GET',
        credentials: "include",
      }).then(response => {
        return response.json();
      }).then(login => {
        userHasAuthenticated(login);
        setTipoUsuario(login.id_tipousuario)
        //props.history.push('/paciente/1/10')
      }).catch(err => {
        props.history.push('/')
      });
    }

    onLoad();
  }, [props.history]);


  function handleLogout() {
    fetch(`http://localhost:8082/session/`, {
      method: 'DELETE',
      credentials: "include",
    }).then(response => {
      if (response.status === 200) {
        userHasAuthenticated("");
        setTipoUsuario(null)
        props.history.push('/')
      }
    })
  }

  return (
    <div className="App">
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand>AdisanFCT</Navbar.Brand>
        <Navbar.Collapse>
        <Navbar.Text>
            {paginaApp}
        </Navbar.Text>
        </Navbar.Collapse>
        <Navbar.Collapse className="justify-content-end">
          <Nav  as="ul">>
          {isAuthenticated ? (<>
            <Nav.Item><Navbar.Text>{isAuthenticated.nombre + " " + isAuthenticated.primer_apellido + " " + isAuthenticated.segundo_apellido}</Navbar.Text></Nav.Item> 

            <Nav.Item><Button className="btn btn-secondary" onClick={handleLogout}>Logout <i className="fas fa-sign-out-alt"></i></Button></Nav.Item>
          </>) : (<></>)
          }
          </Nav>
        </Navbar.Collapse>
      </Navbar>

      <br/>
      <div className="container">
        <Routes appProps={{ isAuthenticated, userHasAuthenticated, tipo_usuario, setTipoUsuario, paginaApp, setPaginaApp }} />
      </div>
    </div>
  );
}

export default withRouter(App);