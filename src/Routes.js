import React from "react";
import { Route, Switch } from "react-router-dom";
import AppliedRoute from "./components/AppliedRoute";
import NotFound from "./containers/notfound/notfound";
import Login from "./containers/login/login";
import Paciente from "./containers/plist/paciente";
import Episodio from "./containers/plist/episodio";
import ViewEpisodio from "./containers/view/episodio";
import ViewAnalitica from "./containers/view/analiticas";
import ViewProcedimiento from "./containers/view/procedimiento";
import ViewTratamiento from "./containers/view/tratamiento";
import NewAnalitica from "./containers/new/analiticas";
import NewProcedimiento from "./containers/new/procedimiento";
import NewTratamiento from "./containers/new/tratamiento";
import UpdateAnalitica from "./containers/update/analiticas";
import UpdateProcedimiento from "./containers/update/procedimiento";
import UpdateTratamiento from "./containers/update/tratamiento";

export default function Routes({ appProps }) {
  return (
    <Switch>
      <AppliedRoute path="/" exact component={Login} appProps={appProps}/>
      <AppliedRoute path={["/paciente/:page/:rpp","/paciente/:page/:rpp/:filter" ,"/paciente/:page/:rpp/:col/:order","/paciente/:page/:rpp/:filter/:col/:order"]} exact component={Paciente} appProps={appProps}/>
      <AppliedRoute path={["/episodio/:paciente/:page/:rpp", "/episodio/:paciente/:page/:rpp/:col/:order"]} exact component={Episodio} appProps={appProps}/>
      
      <AppliedRoute path="/episodio/:paciente/:id" exact component={ViewEpisodio} appProps={appProps}/>
      <AppliedRoute path="/analitica/view/:paciente/:episodio/:analitica" exact component={ViewAnalitica} appProps={appProps}/>
      <AppliedRoute path="/tratamiento/view/:paciente/:episodio/:tratamiento" exact component={ViewTratamiento} appProps={appProps}/>
      <AppliedRoute path="/procedimiento/view/:paciente/:episodio/:procedimiento" exact component={ViewProcedimiento} appProps={appProps}/>

      <AppliedRoute path="/analitica/new/:paciente/:episodio" exact component={NewAnalitica} appProps={appProps}/>
      <AppliedRoute path="/tratamiento/new/:paciente/:episodio" exact component={NewTratamiento} appProps={appProps}/>
      <AppliedRoute path="/procedimiento/new/:paciente/:episodio" exact component={NewProcedimiento} appProps={appProps}/>

      <AppliedRoute path="/analitica/update/:paciente/:episodio/:id" exact component={UpdateAnalitica} appProps={appProps}/>
      <AppliedRoute path="/tratamiento/update/:paciente/:episodio/:id" exact component={UpdateTratamiento} appProps={appProps}/>
      <AppliedRoute path="/procedimiento/update/:paciente/:episodio/:id" exact component={UpdateProcedimiento} appProps={appProps}/>

      <Route component={NotFound} />
    </Switch>
  );
}