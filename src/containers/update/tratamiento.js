import React, { useState, useEffect } from "react";
import { Button, Form, FormControl, Navbar } from "react-bootstrap";
import Select from 'react-select';

import { Link } from "react-router-dom";

export default function Tratamiento(props) {
  const [id, setId] = useState("");
  const [fecha_inicio, setFechaInicio] = useState("");
  const [fecha_fin, setFechaFin] = useState("");
  const [cuidados, setCuidados] = useState("");
  const [importe, setImporte] = useState("");
  const [id_medicamento, setIdMedicamento] = useState("");
  const [medicamentoSelect, setMedicamentoSelect] = useState("");
  const [id_posologia, setIdPosologia] = useState("");
  const [posologiaSelect, setPosologiaSelect] = useState("");
  const [id_via, setIdVia] = useState("");
  const [viaSelect, setViaSelect] = useState("");
  const [id_episodio, setIdEpisodio] = useState(props.match.params.episodio);
  const [id_usuario, setIdUsuario] = useState("");
  const [operacion, setOperacion] = useState(true);
  const [resultado, setResultado] = useState(false);
  const [loading, setLoading] = useState(true);
//  const validTel = RegExp(/^\d{9}$/i);


useEffect(() => {
  if(loading){
  props.setPaginaApp(<><Link to="/paciente/1/10">Pacientes</Link> > 
  <Link to={`/episodio/${props.match.params.paciente}/1/10`}>Ep. del paciente {props.match.params.paciente}</Link> > 
  <Link to={`/episodio/${props.match.params.paciente}/${props.match.params.episodio}`}>Episodio {props.match.params.episodio}</Link> > 
<Navbar.Brand>Edit Tratamiento {props.match.params.id}</Navbar.Brand></>)

async function onLoad() {
  fetch(`http://localhost:8082/tratamiento/get/${props.match.params.id}`, {
    method: 'GET',
    credentials: "include",
  }).then(response => {
    return response.json();
  }).then(tratamiento => {
    setId(tratamiento.id)
    setFechaInicio(new Date(tratamiento.fecha_inicio).toISOString().slice(0,10))
    setFechaFin(new Date(tratamiento.fecha_fin).toISOString().slice(0,10))
    setCuidados(tratamiento.cuidados)
    setImporte(tratamiento.importe)
    setIdMedicamento({label: tratamiento.id_medicamento.descripcion, value: tratamiento.id_medicamento.id})
    setIdPosologia({label: tratamiento.id_posologia.descripcion, value: tratamiento.id_posologia.id})
    setIdVia({label: tratamiento.id_via.via, value: tratamiento.id_via.id})
    setIdEpisodio(tratamiento.id_episodio)
    setIdUsuario(tratamiento.id_usuario.id)
  }).catch(err => err);
}
fetch(`http://localhost:8082/medicamento/getall`, {
  method: 'GET',
  credentials: "include",
}).then(response => {
  return response.json();
}).then(lista => {
  var itemsSelect = []
  lista.map(elemento =>
    itemsSelect.push({ label: elemento.descripcion, value: elemento.id })
  )
  setMedicamentoSelect(itemsSelect)
}).catch(err => err);

fetch(`http://localhost:8082/posologia/getall`, {
  method: 'GET',
  credentials: "include",
}).then(response => {
  return response.json();
}).then(lista => {
  var itemsSelect = []
  lista.map(elemento =>
    itemsSelect.push({ label: elemento.descripcion, value: elemento.id })
  )
  setPosologiaSelect(itemsSelect)
}).catch(err => err);

fetch(`http://localhost:8082/via/getall`, {
  method: 'GET',
  credentials: "include",
}).then(response => {
  return response.json();
}).then(lista => {
  var itemsSelect = []
  lista.map(elemento =>
    itemsSelect.push({ label: elemento.via, value: elemento.id })
  )
  setViaSelect(itemsSelect)
}).catch(err => err);
onLoad();
setLoading(false)
  }
}, [loading, props]);

  

  function validateForm() {
    return true;
  }

  function handleSubmit(event) {
    event.preventDefault();
    var data = {
      id: id,
      fecha_inicio: new Date(fecha_inicio).toString(),
      fecha_fin: new Date(fecha_fin).toString(),
      cuidados: cuidados,
      importe: importe,
      id_medicamento: id_medicamento.value,
      id_posologia: id_posologia.value,
      id_via: id_via.value,
      id_episodio: id_episodio,
      id_usuario: id_usuario
    }
    fetch(`http://localhost:8082/tratamiento/`, {
      method: 'PUT',
      body: JSON.stringify(data),
      credentials: "include",
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status !== 200) {
        setResultado(false)
        setOperacion(false);
      } else {
        return response.json();
      }
    }).then(login => {
      if (login) {
        setResultado(login)
        setOperacion(false);
      }
    }).catch(err => err);
  }

  return (
    <div>
      {operacion ? (<>
        <Form onSubmit={handleSubmit}>
            <div className="row">
            <Form.Group controlId="fecha_inicio" bssize="large" className="col-md-6" validationstate={fecha_inicio.length > 0 ? ("error") : ("")}>
              <Form.Label>Fecha Inicio</Form.Label>
              <Form.Control
                value={fecha_inicio}
                onChange={e => setFechaInicio(e.target.value)}
                type="date"
                name="dni"
              />
              <FormControl.Feedback />
            </Form.Group>
            <Form.Group controlId="fecha_fin" bssize="large" className="col-md-6 date" validationstate={fecha_fin.length > 0 ? ("error") : ("")}>
              <Form.Label>Fecha Fin</Form.Label>
              <Form.Control
                value={fecha_fin}
                onChange={e => setFechaFin(e.target.value)}
                type="date"
                name="dni"
              />
              <FormControl.Feedback />
            </Form.Group>
            </div>
            <div className="row">
            <Form.Group controlId="cuidados" bssize="large" className="col-md-6" validationstate={cuidados.length > 0 ? ("error") : ("")}>
              <Form.Label>Cuidados</Form.Label>
              <Form.Control
                value={cuidados}
                onChange={e => setCuidados(e.target.value)}
                type="text"
                name="dni"
              />
              <FormControl.Feedback />
            </Form.Group>
            <Form.Group controlId="importe" bssize="large" className="col-md-6 " validationstate={importe.length > 0 ? ("error") : ("")}>
              <Form.Label>Importe</Form.Label>
              <Form.Control
                value={importe}
                onChange={e => setImporte(e.target.value)}
                type="text"
                name="dni"
              />
              <FormControl.Feedback />
            </Form.Group>
            </div>
            <div className="row">
            <Form.Group controlId="cuidados" bssize="large" className="col-md-6" validationstate={cuidados.length > 0 ? ("error") : ("")}>
              <Form.Label>Medicamento</Form.Label>
              <Select placeholder="Seleccione una opcion" value={id_medicamento} options={medicamentoSelect} onChange={e => setIdMedicamento({value: e.value, label: e.label})} />
              <FormControl.Feedback />
            </Form.Group>
            <Form.Group controlId="importe" bssize="large" className="col-md-6 " validationstate={importe.length > 0 ? ("error") : ("")}>
              <Form.Label>Posologia</Form.Label>
              <Select placeholder="Seleccione una opcion" value={id_posologia} options={posologiaSelect} onChange={e => setIdPosologia({value: e.value, label: e.label})} />
              <FormControl.Feedback />
            </Form.Group>
          </div>
          <div className="row">
            <Form.Group controlId="cuidados" bssize="large" className="col-md-6" validationstate={cuidados.length > 0 ? ("error") : ("")}>
              <Form.Label>Via</Form.Label>
              <Select placeholder="Seleccione una opcion" value={id_via} options={viaSelect} onChange={e => setIdVia({value: e.value, label: e.label})} />
              <FormControl.Feedback />
            </Form.Group>
          </div>
            <Button block bssize="large" disabled={!validateForm()} type="submit">
              Crear
            </Button>
        </Form>
      </>) : (resultado ? (<>
          <div className="alert alert-success text-center">
            <h2>Datos actualizados con exito</h2>
          </div>
          <div className="text-center">
            <Link className="btn btn-secondary" to={{ pathname: `/episodio/${props.match.params.paciente}/${props.match.params.episodio}` }}>Volver</Link>
          </div>
        </>) : (<>
          <div className="alert alert-danger text-center">
            <h2>Fallo al actualizar, pruebe mas tarde</h2>
          </div>
          <div className="text-center">
            <Link className="btn btn-secondary" to={{ pathname: `/episodio/${props.match.params.paciente}/${props.match.params.episodio}` }}>Volver</Link>
          </div>
        </>))}
      </div>
      );
}