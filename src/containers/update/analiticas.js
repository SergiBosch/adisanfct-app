import React, { useState, useEffect } from "react";
import { Button, Form, FormControl, Navbar } from "react-bootstrap";
import Select from 'react-select';
import { Link } from "react-router-dom";

export default function Analitica(props) {
  const [id, setId] = useState("")
  const [descripcion, setDescripcion] = useState("");
  const [fecha_prevista, setFechaPrevista] = useState("");
  const [fecha_realizacion, setFechaRealizacion] = useState("");
  const [ubicacion, setUbicacion] = useState("");
  const [id_medico, setIdMedico] = useState("");
  const [medicoSelect, setMedicoSelect] = useState("");
  const [id_dependencia, setIdDependencia] = useState("");
  const [dependenciaSelect, setDependenciaSelect] = useState("");
  const [id_catalogoanaliticas, setIdCatalogoanaliticas] = useState("");
  const [catalogoanaliticasSelect, setCatalogoanaliticasSelect] = useState("");
  const [importe, setImporte] = useState("");
  const [id_episodio, setIdEpisodio] = useState(props.match.params.episodio);
  const [id_usuario, setIdUsuario] = useState("");
  const [operacion, setOperacion] = useState(true);
  const [resultado, setResultado] = useState(false);
  const [loading, setLoading] = useState(true);
//  const validTel = RegExp(/^\d{9}$/i);


useEffect(() => {
  if(loading){
  props.setPaginaApp(<><Link to="/paciente/1/10">Pacientes</Link> > 
  <Link to={`/episodio/${props.match.params.paciente}/1/10`}>Ep. del paciente {props.match.params.paciente}</Link> > 
  <Link to={`/episodio/${props.match.params.paciente}/${props.match.params.episodio}`}>Episodio {props.match.params.episodio}</Link> > 
  <Navbar.Brand>Edit Analítica {props.match.params.id}</Navbar.Brand></>)

async function onLoad() {
  fetch(`http://localhost:8082/analitica/get/${props.match.params.id}`, {
    method: 'GET',
    credentials: "include",
  }).then(response => {
    return response.json();
  }).then(analitica => {
    setId(analitica.id)
    setDescripcion(analitica.descripcion)
    setFechaPrevista(new Date(analitica.fecha_prevista).toISOString().slice(0,10))
    setFechaRealizacion(new Date(analitica.fecha_realizacion).toISOString().slice(0,10))
    setUbicacion(analitica.ubicacion)
    setIdMedico({ label: analitica.id_medico.nombre+" "+analitica.id_medico.primer_apellido+" "+analitica.id_medico.segundo_apellido, value: analitica.id_medico.id})
    setIdDependencia({ label: analitica.id_dependencia.descripcion, value: analitica.id_dependencia.id})
    setIdCatalogoanaliticas({ label: analitica.id_catalogoanaliticas.descripcion, value: analitica.id_catalogoanaliticas.id})
    setImporte(analitica.importe)
    setIdEpisodio(analitica.id_episodio)
    setIdUsuario(analitica.id_usuario.id)
  }).catch(err => err);
}
fetch(`http://localhost:8082/catalogoanaliticas/getall`, {
      method: 'GET',
      credentials: "include",
    }).then(response => {
      return response.json();
    }).then(lista => {
      var itemsSelect = []
      lista.map(elemento =>
        itemsSelect.push({ label: elemento.descripcion, value: elemento.id })
      )
      setCatalogoanaliticasSelect(itemsSelect)
    }).catch(err => err);

    fetch(`http://localhost:8082/dependencia/getfilter/${props.isAuthenticated.id_centrosanitario}`, {
      method: 'GET',
      credentials: "include",
    }).then(response => {
      return response.json();
    }).then(lista => {
      var itemsSelect = []
      lista.map(elemento =>
        itemsSelect.push({ label: elemento.descripcion, value: elemento.id })
      )
      setDependenciaSelect(itemsSelect)
    }).catch(err => err);

    fetch(`http://localhost:8082/medico/getfilter/${props.isAuthenticated.id_centrosanitario}`, {
      method: 'GET',
      credentials: "include",
    }).then(response => {
      return response.json();
    }).then(lista => {
      var itemsSelect = []
      lista.map(elemento =>
        itemsSelect.push({ label: elemento.nombre+" "+elemento.primer_apellido+" "+elemento.segundo_apellido, value: elemento.id })
      )
      setMedicoSelect(itemsSelect)
    }).catch(err => err);

onLoad();
setLoading(false)
  }
}, [loading, props]);


  function validateForm() {
    return true;
  }

  function handleSubmit(event) {
    event.preventDefault();
    var data = {
      id: id,
      descripcion: descripcion,
      fecha_prevista: new Date(fecha_prevista).toString(),
      fecha_realizacion: new Date(fecha_realizacion).toString(),
      ubicacion: ubicacion,
      id_medico: id_medico.value,
      id_dependencia: id_dependencia.value,
      id_catalogoanaliticas: id_catalogoanaliticas.value,
      importe: importe,
      id_episodio: id_episodio,
      id_usuario: id_usuario
    }
    fetch(`http://localhost:8082/analitica/`, {
      method: 'PUT',
      body: JSON.stringify(data),
      credentials: "include",
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status !== 200) {
        setResultado(false)
        setOperacion(false);
      } else {
        return response.json();
      }
    }).then(login => {
      if (login) {
        setResultado(login)
        setOperacion(false);
      }
    }).catch(err => err);
  }

  return (
    <div>
      {operacion ? (<>
        <Form onSubmit={handleSubmit}>
          <div className="row">
            <Form.Group controlId="descripcion" bssize="large" className="col-md-12" validationstate={descripcion.length > 0 ? ("error") : ("")}>
              <Form.Label>Descripción</Form.Label>
              <Form.Control
                value={descripcion}
                onChange={e => setDescripcion(e.target.value)}
                type="text"
                name="dni"
              />
              <FormControl.Feedback />
            </Form.Group>
            </div>
            <div className="row">
            <Form.Group controlId="fecha_prevista" bssize="large" className="col-md-6" validationstate={fecha_prevista.length > 0 ? ("error") : ("")}>
              <Form.Label>Fecha Prevista</Form.Label>
              <Form.Control
                value={fecha_prevista}
                onChange={e => setFechaPrevista(e.target.value)}
                type="date"
                name="dni"
              />
              <FormControl.Feedback />
            </Form.Group>
            <Form.Group controlId="fecha_realizacion" bssize="large" className="col-md-6 date" validationstate={fecha_realizacion.length > 0 ? ("error") : ("")}>
              <Form.Label>Fecha Realizacion</Form.Label>
              <Form.Control
                value={fecha_realizacion}
                onChange={e => setFechaRealizacion(e.target.value)}
                type="date"
                name="dni"
              />
              <FormControl.Feedback />
            </Form.Group>
            </div>
            <div className="row">
            <Form.Group controlId="ubicacion" bssize="large" className="col-md-6" validationstate={ubicacion.length > 0 ? ("error") : ("")}>
              <Form.Label>Ubicacion</Form.Label>
              <Form.Control
                value={ubicacion}
                onChange={e => setUbicacion(e.target.value)}
                type="text"
                name="dni"
              />
              <FormControl.Feedback />
            </Form.Group>
            <Form.Group controlId="importe" bssize="large" className="col-md-6 " validationstate={importe.length > 0 ? ("error") : ("")}>
              <Form.Label>Importe</Form.Label>
              <Form.Control
                value={importe}
                onChange={e => setImporte(e.target.value)}
                type="text"
                name="dni"
              />
              <FormControl.Feedback />
            </Form.Group>
            </div>
            <div className="row">
            <Form.Group controlId="catalogoanaliticas" bssize="large" className="col-md-6" validationstate={ubicacion.length > 0 ? ("error") : ("")}>
              <Form.Label>Catalogoanaliticas</Form.Label>
              <Select placeholder="Seleccione una opcion" value={id_catalogoanaliticas} options={catalogoanaliticasSelect} onChange={e => setIdCatalogoanaliticas({value: e.value, label: e.label})} />
              <FormControl.Feedback />
            </Form.Group>
            <Form.Group controlId="dependencia" bssize="large" className="col-md-6 " validationstate={importe.length > 0 ? ("error") : ("")}>
              <Form.Label>Dependencia</Form.Label>
              <Select placeholder="Seleccione una opcion" value={id_dependencia} options={dependenciaSelect} onChange={e => setIdDependencia({value: e.value, label: e.label})} />
            </Form.Group>
            </div>
            <div className="row">
            <Form.Group controlId="medico" bssize="large" className="col-md-6" validationstate={ubicacion.length > 0 ? ("error") : ("")}>
              <Form.Label>Medico</Form.Label>
              <Select placeholder="Seleccione una opcion" value={id_medico} options={medicoSelect} onChange={e => setIdMedico({value: e.value, label: e.label})} />
              <FormControl.Feedback />
            </Form.Group>

            </div>
            <Button block bssize="large" disabled={!validateForm()} type="submit">
              Crear
            </Button>
        
        </Form>
      </>) : (resultado ? (<>
          <div className="alert alert-success text-center">
            <h2>Datos actualizados con exito</h2>
          </div>
          <div className="text-center">
            <Link className="btn btn-secondary" to={{ pathname: `/episodio/${props.match.params.paciente}/${props.match.params.episodio}` }}>Volver</Link>
          </div>
        </>) : (<>
          <div className="alert alert-danger text-center">
            <h2>Fallo al actualizar, pruebe mas tarde</h2>
          </div>
          <div className="text-center">
            <Link className="btn btn-secondary" to={{ pathname: `/episodio/${props.match.params.paciente}/${props.match.params.episodio}` }}>Volver</Link>
          </div>
        </>))}
      </div>
      );
}