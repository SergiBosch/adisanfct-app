import React, { useState, useEffect } from "react";
import { Button, Form, FormControl, Navbar } from "react-bootstrap";
import Select from 'react-select';
import { Link } from "react-router-dom";

export default function Procedimiento(props) {
  const [id, setId] = useState("");
  const [fecha_inicio, setFechaInicio] = useState("");
  const [fecha_fin, setFechaFin] = useState("");
  const [id_instrumentalista, setIdInstrumentalista] = useState("");
  const [instrumentalistaSelect, setInstrumentalistaSelect] = useState("");
  const [id_episodio, setIdEpisodio] = useState(props.match.params.episodio);
  const [id_medico, setIdMedico] = useState("");
  const [medicoSelect, setMedicoSelect] = useState("");
  const [id_dependencia, setIdDependencia] = useState("");
  const [dependenciaSelect, setDependenciaSelect] = useState("");
  const [informe, setInforme] = useState("");
  const [diagnostico_inicial, setDiagnosticoInicial] = useState("");
  const [diagnostico_final, setDiagnosticoFinal] = useState("");
  const [procedimiento_previsto, setProcedimientoPrevisto] = useState("");
  const [procedimiento_realizado, setProcedimientoRealizado] = useState("");
  const [id_prioridad, setIdPrioridad] = useState("");
  const [prioridadSelect, setPrioridadSelect] = useState("");
  const [id_procedimiento, setIdProcedimiento] = useState("");
  const [procedimientoSelect, setProcedimientoSelect] = useState("");
  const [id_usuario, setIdUsuario] = useState("");
  const [operacion, setOperacion] = useState(true);
  const [resultado, setResultado] = useState(false);
  const [loading, setLoading] = useState(true);
  //  const validTel = RegExp(/^\d{9}$/i);


  useEffect(() => {
    if(loading){
    props.setPaginaApp(<><Link to="/paciente/1/10">Pacientes</Link> >
      <Link to={`/episodio/${props.match.params.paciente}/1/10`}>Ep. del paciente {props.match.params.paciente}</Link> >
      <Link to={`/episodio/${props.match.params.paciente}/${props.match.params.episodio}`}>Episodio {props.match.params.episodio}</Link> >
      <Navbar.Brand>Edit Procedimiento {props.match.params.id}</Navbar.Brand></>)

    async function onLoad() {
      fetch(`http://localhost:8082/procedimiento/get/${props.match.params.id}`, {
        method: 'GET',
        credentials: "include",
      }).then(response => {
        return response.json();
      }).then(procedimiento => {
        setId(procedimiento.id)
        setFechaInicio(new Date(procedimiento.fecha_inicio).toISOString().slice(0, 10))
        setFechaFin(new Date(procedimiento.fecha_fin).toISOString().slice(0, 10))
        setIdInstrumentalista({ label: procedimiento.id_instrumentalista.nombre + " " + procedimiento.id_instrumentalista.primer_apellido + " " + procedimiento.id_instrumentalista.segundo_apellido, value: procedimiento.id_instrumentalista.id })
        setIdEpisodio(procedimiento.id_episodio)
        setIdMedico({ label: procedimiento.id_medico.nombre + " " + procedimiento.id_medico.primer_apellido + " " + procedimiento.id_medico.segundo_apellido, value: procedimiento.id_medico.id })
        setIdDependencia({ label: procedimiento.id_dependencia.descripcion, value: procedimiento.id_dependencia.id })
        setInforme(procedimiento.informe)
        setDiagnosticoInicial(procedimiento.diagnostico_inicial)
        setDiagnosticoFinal(procedimiento.diagnostico_final)
        setProcedimientoPrevisto(procedimiento.procedimiento_previsto)
        setProcedimientoRealizado(procedimiento.procedimiento_realizado)
        setIdPrioridad({ label: procedimiento.id_prioridad.descripcion, value: procedimiento.id_prioridad.id })
        setIdProcedimiento({ label: procedimiento.id_procedimiento.descripcion, value: procedimiento.id_procedimiento.id })
        setIdUsuario(procedimiento.id_usuario.id)
      }).catch(err => err);
    }
    fetch(`http://localhost:8082/dependencia/getfilter/${props.isAuthenticated.id_centrosanitario}`, {
      method: 'GET',
      credentials: "include",
    }).then(response => {
      return response.json();
    }).then(lista => {
      var itemsSelect = []
      lista.map(elemento =>
        itemsSelect.push({ label: elemento.descripcion, value: elemento.id })
      )
      setDependenciaSelect(itemsSelect)
    }).catch(err => err);

    fetch(`http://localhost:8082/instrumentalista/getfilter/${props.isAuthenticated.id_centrosanitario}`, {
      method: 'GET',
      credentials: "include",
    }).then(response => {
      return response.json();
    }).then(lista => {
      var itemsSelect = []
      lista.map(elemento =>
        itemsSelect.push({ label: elemento.nombre + " " + elemento.primer_apellido + " " + elemento.segundo_apellido, value: elemento.id })
      )
      setInstrumentalistaSelect(itemsSelect)
    }).catch(err => err);

    fetch(`http://localhost:8082/medico/getfilter/${props.isAuthenticated.id_centrosanitario}`, {
      method: 'GET',
      credentials: "include",
    }).then(response => {
      return response.json();
    }).then(lista => {
      var itemsSelect = []
      lista.map(elemento =>
        itemsSelect.push({ label: elemento.nombre + " " + elemento.primer_apellido + " " + elemento.segundo_apellido, value: elemento.id })
      )
      setMedicoSelect(itemsSelect)
    }).catch(err => err);

    fetch(`http://localhost:8082/catalogoprocedimientos/getall`, {
      method: 'GET',
      credentials: "include",
    }).then(response => {
      return response.json();
    }).then(lista => {
      var itemsSelect = []
      lista.map(elemento =>
        itemsSelect.push({ label: elemento.descripcion, value: elemento.id })
      )
      setProcedimientoSelect(itemsSelect)
    }).catch(err => err);

    fetch(`http://localhost:8082/prioridad/getall`, {
      method: 'GET',
      credentials: "include",
    }).then(response => {
      return response.json();
    }).then(lista => {
      var itemsSelect = []
      lista.map(elemento =>
        itemsSelect.push({ label: elemento.descripcion, value: elemento.id })
      )
      setPrioridadSelect(itemsSelect)
    }).catch(err => err);
    onLoad();
    setLoading(false)
  }
  }, [loading, props]);


  function validateForm() {
    return true;
  }

  function handleSubmit(event) {
    event.preventDefault();
    var data = {
      id: id,
      fecha_inicio: new Date(fecha_inicio).toString(),
      fecha_fin: new Date(fecha_fin).toString(),
      id_instrumentalista: id_instrumentalista.value,
      id_episodio: id_episodio,
      id_medico: id_medico.value,
      id_dependencia: id_dependencia.value,
      id_prioridad: id_prioridad.value,
      informe: informe,
      diagnostico_inicial: diagnostico_inicial,
      diagnostico_final: diagnostico_final,
      procedimiento_previsto: procedimiento_previsto,
      procedimiento_realizado: procedimiento_realizado,
      id_procedimiento: id_procedimiento.value,
      id_usuario: id_usuario
    }
    fetch(`http://localhost:8082/procedimiento/`, {
      method: 'PUT',
      body: JSON.stringify(data),
      credentials: "include",
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      if (response.status !== 200) {
        setResultado(false)
        setOperacion(false);
      } else {
        return response.json();
      }
    }).then(login => {
      if (login) {
        setResultado(login)
        setOperacion(false);
      }
    }).catch(err => err);
  }

  return (
    <div>
      {operacion ? (<>
        <Form onSubmit={handleSubmit}>
          <div className="row">
            <Form.Group controlId="fecha_inicio" bssize="large" className="col-md-6" validationstate={fecha_inicio.length > 0 ? ("error") : ("")}>
              <Form.Label>Fecha Inicio</Form.Label>
              <Form.Control
                value={fecha_inicio}
                onChange={e => setFechaInicio(e.target.value)}
                type="date"
                name="dni"
              />
              <FormControl.Feedback />
            </Form.Group>
            <Form.Group controlId="fecha_realizacion" bssize="large" className="col-md-6 date" validationstate={fecha_fin.length > 0 ? ("error") : ("")}>
              <Form.Label>Fecha Fin</Form.Label>
              <Form.Control
                value={fecha_fin}
                onChange={e => setFechaFin(e.target.value)}
                type="date"
                name="dni"
              />
              <FormControl.Feedback />
            </Form.Group>
          </div>
          <div className="row">
            <Form.Group controlId="diagnostico_inicial" bssize="large" className="col-md-6" validationstate={diagnostico_inicial.length > 0 ? ("error") : ("")}>
              <Form.Label>Diagnostico Inicial</Form.Label>
              <Form.Control
                value={diagnostico_inicial}
                onChange={e => setDiagnosticoInicial(e.target.value)}
                type="text"
                name="dni"
              />
              <FormControl.Feedback />
            </Form.Group>
            <Form.Group controlId="diagnostico_final" bssize="large" className="col-md-6 " validationstate={diagnostico_final.length > 0 ? ("error") : ("")}>
              <Form.Label>Diagnostico Final</Form.Label>
              <Form.Control
                value={diagnostico_final}
                onChange={e => setDiagnosticoFinal(e.target.value)}
                type="text"
                name="dni"
              />
              <FormControl.Feedback />
            </Form.Group>
          </div>
          <div className="row">
            <Form.Group controlId="procedimiento_previsto" bssize="large" className="col-md-6" validationstate={procedimiento_previsto.length > 0 ? ("error") : ("")}>
              <Form.Label>Procedimiento previsto</Form.Label>
              <Form.Control
                value={procedimiento_previsto}
                onChange={e => setProcedimientoPrevisto(e.target.value)}
                type="text"
                name="dni"
              />
              <FormControl.Feedback />
            </Form.Group>
            <Form.Group controlId="procedimiento_realizado" bssize="large" className="col-md-6 " validationstate={procedimiento_realizado.length > 0 ? ("error") : ("")}>
              <Form.Label>Procedimiento Realizado</Form.Label>
              <Form.Control
                value={procedimiento_realizado}
                onChange={e => setProcedimientoRealizado(e.target.value)}
                type="text"
                name="dni"
              />
              <FormControl.Feedback />
            </Form.Group>
          </div>
          <div className="row">
            <Form.Group controlId="id_prioridad" bssize="large" className="col-md-6">
              <Form.Label>Prioridad</Form.Label>
              <Select placeholder="Seleccione una opcion" value={id_prioridad} options={prioridadSelect} onChange={e => setIdPrioridad({ value: e.value, label: e.label })} />
              <FormControl.Feedback />
            </Form.Group>
            <Form.Group controlId="id_procedimiento" bssize="large" className="col-md-6 ">
              <Form.Label>Procedimiento</Form.Label>
              <Select placeholder="Seleccione una opcion" value={id_procedimiento} options={procedimientoSelect} onChange={e => setIdProcedimiento({ value: e.value, label: e.label })} />
              <FormControl.Feedback />
            </Form.Group>
          </div>
          <div className="row">
            <Form.Group controlId="id_instrumentalista" bssize="large" className="col-md-6">
              <Form.Label>Instrumentalista</Form.Label>
              <Select placeholder="Seleccione una opcion" value={id_instrumentalista} options={instrumentalistaSelect} onChange={e => setIdInstrumentalista({ value: e.value, label: e.label })} />
              <FormControl.Feedback />
            </Form.Group>
            <Form.Group controlId="id_medico" bssize="large" className="col-md-6 " >
              <Form.Label>Medico</Form.Label>
              <Select placeholder="Seleccione una opcion" value={id_medico} options={medicoSelect} onChange={e => setIdMedico({ value: e.value, label: e.label })} />
              <FormControl.Feedback />
            </Form.Group>
          </div>
          <div className="row">
            <Form.Group controlId="id_dependencia" bssize="large" className="col-md-6" >
              <Form.Label>Dependencia</Form.Label>
              <Select placeholder="Seleccione una opcion" value={id_dependencia} options={dependenciaSelect} onChange={e => setIdDependencia({ value: e.value, label: e.label })} />
              <FormControl.Feedback />
            </Form.Group>
            <Form.Group controlId="informe" bssize="large" className="col-md-6 " validationstate={informe.length > 0 ? ("error") : ("")}>
              <Form.Label>Informe</Form.Label>
              <Form.Control
                value={informe}
                onChange={e => setInforme(e.target.value)}
                type="text"
                name="dni"
              />
              <FormControl.Feedback />
            </Form.Group>
          </div>
          <Button block bssize="large" disabled={!validateForm()} type="submit">
            Crear
            </Button>

        </Form>
      </>) : (resultado ? (<>
        <div className="alert alert-success text-center">
          <h2>Datos actualizados con exito</h2>
        </div>
        <div className="text-center">
          <Link className="btn btn-secondary" to={{ pathname: `/episodio/${props.match.params.paciente}/${props.match.params.episodio}` }}>Volver</Link>
        </div>
      </>) : (<>
        <div className="alert alert-danger text-center">
          <h2>Fallo al actualizar, pruebe mas tarde</h2>
        </div>
        <div className="text-center">
          <Link className="btn btn-secondary" to={{ pathname: `/episodio/${props.match.params.paciente}/${props.match.params.episodio}` }}>Volver</Link>
        </div>
      </>))}
    </div>
  );
}