import React, { Component } from 'react';
import { Link } from "react-router-dom";

class paginas extends Component {
    _isMounted = false;
    abortController = new AbortController()
    constructor(props) {
        super(props);  
        this.state = {
            paginacion: []
          }     
    }

    UNSAFE_componentWillReceiveProps() {
        var consulta
        if(this.props.tabla === "episodio"){
            consulta = `http://localhost:8082/${this.props.tabla}/countepisodiopaciente/${this.props.paciente}`
        }else{
            if(this.props.filter){
                consulta = `http://localhost:8082/${this.props.tabla}/count/${this.props.filter}`
            }else{
                consulta = `http://localhost:8082/${this.props.tabla}/count`
            }
        }
        fetch(consulta, {signal: this.abortController.signal})
        .then((response) => {
          return response.json()
        })
        .then((registros) => {
            var paginas = []
            var paginaActual = parseInt(this.props.paginaActual)
            var vecindad = 3
            var numPaginas = Math.ceil(registros/this.props.rpp)
            for(var i=0; i < numPaginas; i++){
                var pagina = i+1
                if(pagina === 1){
                    paginas.push(pagina) 
                } else if (pagina > (paginaActual - vecindad) && pagina < (paginaActual + vecindad)){
                    paginas.push(pagina)
                } else if (pagina === numPaginas){
                    paginas.push(pagina)
                } else if (pagina === (paginaActual - vecindad) || pagina === (paginaActual + vecindad) ){
                    paginas.push("...")
                }
            }
            this.setState({paginacion: paginas})
        }).catch(err => {
            if (err.name === "AbortError") return
            throw err
          })
    }

    componentWillUnmount() {
        this.abortController.abort()
        this._isMounted = false;

      }

    render(){
        var i = 0
        return (
            <ul className="pagination">
                {this.state.paginacion.map(pagina => {
                    i++
                    return (
                        <li key={i}  className= {`page-item ${parseInt(this.props.paginaActual) === pagina ? "active" : ""} ${pagina === "..." ? "disabled" : ""}`}>
                            <Link className = {`page-link  `} to={{ pathname: `/${this.props.tabla}${this.props.paciente ? ("/" + this.props.paciente) : ("")}/${pagina}/${this.props.rpp}${this.props.filter ? ("/" + this.props.filter) : ("")}${this.props.col ? ("/" + this.props.col) : ("")}${this.props.order ? ("/" + this.props.order) : ("")}` }}>
                                {pagina} 
                            </Link>
                        </li>
                    );
                })}
            </ul>
          );
    }
}

export default paginas;