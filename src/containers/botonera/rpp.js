import React from "react";
import { Link } from "react-router-dom";


export default function rpp(props) {
  return (
    <ul className="pagination">
      <li className={`page-item ${props.rpp === "10" ? "active" : ""}`}>
        <Link className={`page-link`} to={{ pathname: `/${props.tabla}/1/10` }}>10 </Link>
      </li>
      <li className={`page-item ${props.rpp === "50" ? "active" : ""}`}>
        <Link className={`page-link`} to={{ pathname: `/${props.tabla}/1/50` }}>50 </Link>
      </li>
      <li className={`page-item ${props.rpp === "100" ? "active" : ""}`}>
        <Link className={`page-link`} to={{ pathname: `/${props.tabla}/1/100` }}>100 </Link>
      </li>
    </ul>
  );
}