import React, { useState, useEffect } from "react";
import { Button, Form, Navbar} from "react-bootstrap";
import sha256 from 'js-sha256';


import "./login.css";

export default function Login(props) {
  
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if(loading){
      props.setPaginaApp(<Navbar.Brand>Iniciar session</Navbar.Brand>)
      setLoading(false)
    }
    
  }, [props, loading])

  function validateForm() { 
    return login.length > 0 && password.length > 0;
  }

  function handleSubmit(event) {
    event.preventDefault();
    var data = {
      login: login,
      password: sha256(password).toUpperCase()
    }
    fetch(`http://localhost:8082/session/`,{
      method: 'POST',
      body: JSON.stringify(data),
      credentials: "include",
      headers: {
        'Content-Type': 'application/json'
      }
    }).then( response => {
      return response.json();   
    }).then( login => {
      props.userHasAuthenticated(login);   
      props.setTipoUsuario(login.id_tipousuario);
      props.history.push("/paciente/1/10");  
    }).catch(err => err);
  }

  function admin(){
    setLogin("rafael")
    setPassword("hola")
  }

  return (
    <div className="Login">
      <Form onSubmit={handleSubmit}>
        <Form.Group controlId="login">
          <Form.Label>Username</Form.Label>
          <Form.Control
            autoFocus
            size="lg"
            type="text"
            value={login}
            name="login"
            onChange={e => setLogin(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
          <Form.Control
            value={password}
            size="lg"
            onChange={e => setPassword(e.target.value)}
            type="password"
            name="password"
          />
        </Form.Group>
        <Button variant="outline-primary" size="lg" block disabled={!validateForm()} type="submit">
          Login
        </Button>
        <Button variant="outline-primary" size="lg" block onClick={admin}>
          Admin
        </Button>
      </Form>
    </div>
  );
}