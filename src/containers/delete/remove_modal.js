import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button'

export default function Analitica(props) {
  const [modalShow, setModalShow] = useState(false);

  function eliminar() {
    fetch(`http://localhost:8082/${props.tabla}/${props.idBorrable}`,
    { method: 'delete' })
    .then((response) => {
      return response.json()
    })
    .then((resultado) => {
      if (resultado) {
        props.actualizar(props)
      }
    })
  }

  function MyVerticallyCenteredModal(props) {
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Confirmación
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5>Seguro que quieres eliminar este registro de {props.tabla}?</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => eliminar(true)}>Confirmar</Button>
          <Button onClick={props.onHide}>Cerrar</Button>
        </Modal.Footer>
      </Modal>
    );
  }

    return (
      <>
      <Button className="btn btn-secondary" onClick={() => setModalShow(true)}>
        {props.contenido}
      </Button>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        tabla={props.tabla}
        id={props.id}
      />
    </>
    );
  }