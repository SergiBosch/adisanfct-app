import React, { Component } from 'react';
import Rpp from '../botonera/rpp';
import Paginas from '../botonera/paginas';
import { Link } from "react-router-dom";
import Table from 'react-bootstrap/Table'
import { Navbar } from "react-bootstrap";


class usuario extends Component {
  _isMounted = false;
  abortController = new AbortController()

  constructor(props) {
    super(props);
    this.state = {
      filas: []
    }
  }

  UNSAFE_componentWillMount() {
    this._isMounted = true;
    const { match: { params } } = this.props;
    this.props.setPaginaApp(<><Link to="/paciente/1/10">Pacientes</Link> > <Navbar.Brand>Episodios Paciente: {params.paciente}</Navbar.Brand></>)
    this.consultas(params)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { match: { params } } = nextProps;
    this.consultas(params)
  }
  
  componentWillUnmount() {
    this.abortController.abort()
    this._isMounted = false;
  }

  consultas(params) {
    this.setState({ page: params.page })
    this.setState({ rpp: params.rpp })
    this.setState({ paciente: params.paciente })
    this.setState({ col: params.col })
    this.setState({ order: params.order })
    this.setState({ filter: params.filter })
    this.setState({ filtro: params.filter })
    var consulta
    if (params.col && params.order){
      consulta = `http://localhost:8082/episodio/episodiospaciente/${params.paciente}/${params.page - 1}/${params.rpp}/${params.col}/${params.order}`
    }else {
      consulta = `http://localhost:8082/episodio/episodiospaciente/${params.paciente}/${params.page - 1}/${params.rpp}`
    }
    fetch(consulta, {signal: this.abortController.signal})
      .then((response) => {
        return response.json()
      })
      .then((usuarios) => {
        this.setState({ filas: usuarios })
      }).catch(err => {
        if (err.name === "AbortError") return
        throw err
      })
  }

  render() {
    return (
      <>
        <div className="row">
          <div className="col-md-10">
            <Paginas tabla="episodio" paginaActual={this.state.page} rpp={this.state.rpp} paciente={this.state.paciente} col={this.state.col} order={this.state.order}/>
          </div>
          <div className="col-md-2">
            <Rpp tabla="episodio" rpp={this.state.rpp} />
          </div>
        </div>
        <Table  striped bordered hover>
          <thead>
            <tr>
              <th><Link to={{ pathname: `/episodio/${this.state.paciente}/${this.state.page}/${this.state.rpp}/id/${this.state.col === "id" && this.state.order === "asc" ? ("desc") : ("asc")}` }}>Id {this.state.col === "id" ? (this.state.order === "asc" ? (<i className="fas fa-chevron-up"></i>) : (<i className="fas fa-chevron-down"></i>)) : (<></>)}</Link></th>
              <th><Link to={{ pathname: `/episodio/${this.state.paciente}/${this.state.page}/${this.state.rpp}/firma/${this.state.col === "firma" && this.state.order === "asc" ? ("desc") : ("asc")}` }}>Firma {this.state.col === "firma" ? (this.state.order === "asc" ? (<i className="fas fa-chevron-up"></i>) : (<i className="fas fa-chevron-down"></i>)) : (<></>)}</Link></th>
              <th><Link to={{ pathname: `/episodio/${this.state.paciente}/${this.state.page}/${this.state.rpp}/inicio/${this.state.col === "inicio" && this.state.order === "asc" ? ("desc") : ("asc")}` }}>Inicio {this.state.col === "inicio" ? (this.state.order === "asc" ? (<i className="fas fa-chevron-up"></i>) : (<i className="fas fa-chevron-down"></i>)) : (<></>)}</Link></th>
              <th><Link to={{ pathname: `/episodio/${this.state.paciente}/${this.state.page}/${this.state.rpp}/alta/${this.state.col === "alta" && this.state.order === "asc" ? ("desc") : ("asc")}` }}>Alta {this.state.col === "alta" ? (this.state.order === "asc" ? (<i className="fas fa-chevron-up"></i>) : (<i className="fas fa-chevron-down"></i>)) : (<></>)}</Link></th>
              <th>Medico</th>
              <th>Seleccionar</th>
            </tr>
          </thead>
          <tbody>
            {this.state.filas.map(usuarios => {
              return (
                <tr key={usuarios.id}>
                  <td>{usuarios.id}</td>
                  <td>{new Date(usuarios.fecha_firma).toLocaleDateString()}</td>
                  <td>{new Date(usuarios.fecha_inicio).toLocaleDateString()}</td>
                  <td>{new Date(usuarios.fecha_alta).toLocaleDateString()}</td>
                  <td>{usuarios.id_medico.nombre} {usuarios.id_medico.primer_apellido} {usuarios.id_medico.segundo_apellido}</td>
                  <td className="text-center">
                  <Link className="btn btn-secondary" to={{ pathname: `/episodio/${this.state.paciente}/${usuarios.id}` }}><i className="fas fa-th-list"></i></Link>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </>
    );
  }
}

export default usuario;