import React, { Component } from 'react';
import Rpp from '../botonera/rpp';
import Paginas from '../botonera/paginas';
import { Link } from "react-router-dom";
import { Navbar } from "react-bootstrap";

import Table from 'react-bootstrap/Table'


class usuario extends Component {
  _isMounted = false;
  abortController = new AbortController()
  constructor(props) {
    super(props);
    this.state = {
      filas: []
    }
  }

  UNSAFE_componentWillMount() {
    const { match: { params } } = this.props;
    this.props.setPaginaApp(<Navbar.Brand>Lista Pacientes</Navbar.Brand>)
    this.consultas(params)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { match: { params } } = nextProps;
    this.consultas(params)
  }

  componentWillUnmount() {
    this.abortController.abort()
    this._isMounted = false;
  }

  consultas(params) {
    this.setState({ page: params.page })
    this.setState({ rpp: params.rpp })
    this.setState({ filter: params.filter })
    this.setState({ filtro: params.filter })
    this.setState({ col: params.col })
    this.setState({ order: params.order })
    var consulta

    if (params.filter) {
      if (params.col && params.order) {
        consulta = `http://localhost:8082/paciente/getpage/${params.page - 1}/${params.rpp}/${params.filter}/${params.col}/${params.order}`
      } else {
        consulta = `http://localhost:8082/paciente/getpage/${params.page - 1}/${params.rpp}/${params.filter}`
      }
    } else {
      if (params.col && params.order) {
        consulta = `http://localhost:8082/paciente/getpage/${params.page - 1}/${params.rpp}/${params.col}/${params.order}`
      } else {
        consulta = `http://localhost:8082/paciente/getpage/${params.page - 1}/${params.rpp}`
      }
    }

    fetch(consulta, { signal: this.abortController.signal })
      .then((response) => {
        return response.json()
      })
      .then((pacientes) => {
        this.setState({ filas: pacientes })
      }).catch(err => {
        if (err.name === "AbortError") return
        throw err
      })
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col-md-4">
            <Paginas tabla="paciente" paginaActual={this.state.page} rpp={this.state.rpp} filter={this.state.filter} col={this.state.col} order={this.state.order}/>
          </div>
          <div className="col-md-6">
            <div className="input-group mb-3">
              <input type="text" className="form-control" placeholder="Buscar por id, nombre y DNI" aria-describedby="basic-addon2" value={this.state.filtro} onChange={e => this.setState({filtro : e.target.value})}/>
                <div className="input-group-append">
                  <Link className="btn btn-primary" type="button" to={`/paciente/${this.state.page}/${this.state.rpp}${this.state.filtro ? ("/" + this.state.filtro) : ("")}${this.state.col ? ("/" + this.state.col) : ("")}${this.state.order ? ("/" + this.state.order) : ("")}`}>Buscar</Link>
                </div>
              </div>
            </div>
            <div className="col-md-2">
              <Rpp tabla="paciente" rpp={this.state.rpp} />
            </div>
          </div>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th><Link to={{ pathname: `/paciente/${this.state.page}/${this.state.rpp}${this.state.filter ? ("/" + this.state.filter) : ("")}/id/${this.state.col === "id" && this.state.order === "asc" ? ("desc") : ("asc")}` }}>Id {this.state.col === "id" ? (this.state.order === "asc" ? (<i className="fas fa-chevron-up"></i>) : (<i className="fas fa-chevron-down"></i>)) : (<></>)}</Link></th>
                <th><Link to={{ pathname: `/paciente/${this.state.page}/${this.state.rpp}${this.state.filter ? ("/" + this.state.filter) : ("")}/dni/${this.state.col === "dni" && this.state.order === "asc" ? ("desc") : ("asc")}` }}>DNI {this.state.col === "dni" ? (this.state.order === "asc" ? (<i className="fas fa-chevron-up"></i>) : (<i className="fas fa-chevron-down"></i>)) : (<></>)}</Link></th>
                <th><Link to={{ pathname: `/paciente/${this.state.page}/${this.state.rpp}${this.state.filter ? ("/" + this.state.filter) : ("")}/nombre/${this.state.col === "nombre" && this.state.order === "asc" ? ("desc") : ("asc")}` }}>Nombre {this.state.col === "nombre" ? (this.state.order === "asc" ? (<i className="fas fa-chevron-up"></i>) : (<i className="fas fa-chevron-down"></i>)) : (<></>)}</Link></th>
                <th><Link to={{ pathname: `/paciente/${this.state.page}/${this.state.rpp}${this.state.filter ? ("/" + this.state.filter) : ("")}/apellidos/${this.state.col === "apellidos" && this.state.order === "asc" ? ("desc") : ("asc")}` }}>Apellidos {this.state.col === "apellidos" ? (this.state.order === "asc" ? (<i className="fas fa-chevron-up"></i>) : (<i className="fas fa-chevron-down"></i>)) : (<></>)}</Link></th>
                <th><Link to={{ pathname: `/paciente/${this.state.page}/${this.state.rpp}${this.state.filter ? ("/" + this.state.filter) : ("")}/email/${this.state.col === "email" && this.state.order === "asc" ? ("desc") : ("asc")}` }}>Email {this.state.col === "email" ? (this.state.order === "asc" ? (<i className="fas fa-chevron-up"></i>) : (<i className="fas fa-chevron-down"></i>)) : (<></>)}</Link></th>
                <th>Seleccionar</th>
              </tr>
            </thead>
            <tbody>
              {this.state.filas.map(paciente => {
                return (
                  <tr key={paciente.id}>
                    <td>{paciente.id}</td>
                    <td>{paciente.dni}</td>
                    <td>{paciente.nombre}</td>
                    <td>{paciente.primer_apellido} {paciente.segundo_apellido}</td>
                    <td>{paciente.email}</td>
                    <td className="text-center">
                      <Link className="btn btn-secondary" to={{ pathname: `/episodio/${paciente.id}/1/10` }}><i className="fas fa-th-list"></i></Link>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </div>
    );
  }
}

export default usuario;