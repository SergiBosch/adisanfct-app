import React, { useState, useEffect } from "react";
import { Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";
import Table from 'react-bootstrap/Table';


export default function Tratamiento(props) {
  const [id, setId] = useState("");
  const [fecha_inicio, setFechaInicio] = useState("");
  const [fecha_fin, setFechaFin] = useState("");
  const [cuidados, setCuidados] = useState("");
  const [importe, setImporte] = useState("");
  const [id_medicamento, setIdMedicamento] = useState(null);
  const [id_posologia, setIdPosologia] = useState(null);
  const [id_via, setIdVia] = useState(null);
  const [id_episodio, setIdEpisodio] = useState(props.episodio);
  const [loading, setLoading] = useState(true);
  //  const validTel = RegExp(/^\d{9}$/i);


  useEffect(() => {
    if(loading){
    props.setPaginaApp(<><Link to="/paciente/1/10">Pacientes</Link> >
      <Link to={`/episodio/${props.match.params.paciente}/1/10`}>Ep. del paciente {props.match.params.paciente}</Link> >
      <Link to={`/episodio/${props.match.params.paciente}/${props.match.params.episodio}`}>Episodio {props.match.params.episodio}</Link> >
      <Navbar.Brand>Tratamiento {props.match.params.tratamiento}</Navbar.Brand></>)

    async function onLoad() {
      fetch(`http://localhost:8082/tratamiento/get/${props.match.params.tratamiento}`, {
        method: 'GET',
        credentials: "include",
      }).then(response => {
        return response.json();
      }).then(tratamiento => {
        setId(tratamiento.id)
        setFechaInicio(tratamiento.fecha_inicio)
        setFechaFin(tratamiento.fecha_fin)
        setCuidados(tratamiento.cuidados)
        setImporte(tratamiento.importe)
        setIdMedicamento(tratamiento.id_medicamento)
        setIdPosologia(tratamiento.id_posologia)
        setIdVia(tratamiento.id_via)
        setIdEpisodio(tratamiento.id_episodio)
      }).catch(err => err);
    }
    onLoad();
    setLoading(false)
  }
  }, [loading, props]);

  return (
    <div>
      <Table striped bordered hover>
        <tbody>
          <tr>
            <th>Id</th>
            <td>{id}</td>
          </tr>
          <tr>
            <th>Fecha Inicio</th>
            <td>{new Date(fecha_inicio).toLocaleDateString()}</td>
          </tr>
          <tr>
            <th>Fecha Fin</th>
            <td>{new Date(fecha_fin).toLocaleDateString()}</td>
          </tr>
          <tr>
            <th>Cuidados</th>
            <td>{cuidados}</td>
          </tr>
          <tr>
            <th>Importe</th>
            <td>{importe}€</td>
          </tr>
          <tr>
            <th>Medicamento</th>
          <td>{id_medicamento ? (<>{id_medicamento.descripcion}</>):(<>No hay nada asignado a este campo</>)}</td>
          </tr>
          <tr>
            <th>Posologia</th>
            <td>{id_posologia ? (<>{id_posologia.descripcion}</>):(<>No hay nada asignado a este campo</>)}</td>
          </tr>
          <tr>
            <th>Via</th>
            <td>{id_via ? (<>{id_via.via}</>):(<>No hay nada asignado a este campo</>)}</td>
          </tr>
          <tr>
            <th>Episodio</th>
            <td>{id_episodio}</td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
}