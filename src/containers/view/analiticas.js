import React, { useState, useEffect } from "react";
import { Table, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Analitica(props) {
  const [id, setId] = useState("");
  const [descripcion, setDescripcion] = useState("");
  const [fecha_prevista, setFechaPrevista] = useState("");
  const [fecha_realizacion, setFechaRealizacion] = useState("");
  const [ubicacion, setUbicacion] = useState("");
  const [id_medico, setIdMedico] = useState("");
  const [id_dependencia, setIdDependencia] = useState("");
  const [id_catalogoanaliticas, setIdCatalogoanaliticas] = useState("");
  const [importe, setImporte] = useState("");
  const [id_episodio, setIdEpisodio] = useState(props.episodio);
  const [loading, setLoading] = useState(true);
//  const validTel = RegExp(/^\d{9}$/i);


useEffect(() => {
  if(loading){
  props.setPaginaApp(<><Link to="/paciente/1/10">Pacientes</Link> > 
  <Link to={`/episodio/${props.match.params.paciente}/1/10`}>Ep. del paciente {props.match.params.paciente}</Link> > 
  <Link to={`/episodio/${props.match.params.paciente}/${props.match.params.episodio}`}>Episodio {props.match.params.episodio}</Link> > 
  <Navbar.Brand>Analítica {props.match.params.analitica}</Navbar.Brand></>)

async function onLoad() {
  fetch(`http://localhost:8082/analitica/get/${props.match.params.analitica}`, {
    method: 'GET',
    credentials: "include",
  }).then(response => {
    return response.json();
  }).then(analitica => {
    setId(analitica.id)
    setDescripcion(analitica.descripcion)
    setFechaPrevista(analitica.fecha_prevista)
    setFechaRealizacion(analitica.fecha_realizacion)
    setUbicacion(analitica.ubicacion)
    setIdMedico(analitica.id_medico)
    setIdDependencia(analitica.id_dependencia)
    setIdCatalogoanaliticas(analitica.id_catalogoanaliticas)
    setImporte(analitica.importe)
    setIdEpisodio(analitica.id_episodio)
  }).catch(err => err);

}

onLoad();
setLoading(false);
  }
}, [props, loading]);

  return (
    <div>
    <Table striped bordered hover>
      <tbody>
        <tr>
          <th>Id</th>
          <td>{id}</td>
        </tr>
        <tr>
          <th>Descripcion</th>
          <td>{descripcion}</td>
        </tr>
        <tr>
          <th>Fecha Prevista</th>
          <td>{new Date(fecha_prevista).toLocaleDateString()}</td>
        </tr>
        <tr>
          <th>Fecha Realizacion</th>
          <td>{new Date(fecha_realizacion).toLocaleDateString()}</td>
        </tr>
        <tr>
          <th>Ubicacion</th>
          <td>{ubicacion}</td>
        </tr>
        <tr>
          <th>Medico</th>
          <td>{id_medico ? (<>{id_medico.nombre} {id_medico.primer_apellido} {id_medico.segundo_apellido}</>):(<>No hay nada asignado a este campo</>)}</td>
        </tr>
        <tr>
          <th>Dependencia</th>
          <td>{id_dependencia ? (<>{id_dependencia.descripcion}</>):(<>No hay nada asignado a este campo</>)}</td>
        </tr>
        <tr>
          <th>Catalogoanaliticas</th>
          <td>{id_catalogoanaliticas ? (<>{id_catalogoanaliticas.descripcion}</>):(<>No hay nada asignado a este campo</>)}</td>
        </tr>
        <tr>
          <th>Importe</th>
          <td>{importe}€</td>
        </tr>
        <tr>
          <th>Episodio</th>
          <td>{id_episodio}</td>
        </tr>
      </tbody>
    </Table>
  </div>
      );
}