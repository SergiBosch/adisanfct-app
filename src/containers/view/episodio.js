import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { Navbar } from "react-bootstrap";
import RemoveModal from "../delete/remove_modal";
import Table from 'react-bootstrap/Table';


class episodio extends Component {
  constructor(props) {
    super(props);
    this.actualizar = this.actualizar.bind(this)
    this.state = {
      filas: [],
      tratamiento: true,
      analitica: false,
      procedimiento: false
    }
  }

  UNSAFE_componentWillMount() {
    const { match: { params } } = this.props;
    this.props.setPaginaApp(<><Link to="/paciente/1/10">Pacientes</Link> > <Link to={`/episodio/${params.paciente}/1/10`}>Ep. del paciente {params.paciente}</Link> > <Navbar.Brand>Episodio {params.id}</Navbar.Brand></>)
    this.consultas(params)
  }

  actualizar(params) {
    this.consultas(params)
  }

  consultas(params) {
    fetch(`http://localhost:8082/episodio/get/${params.id}`)
      .then((response) => {
        return response.json()
      })
      .then((episodio) => {
        if (episodio.lista_tratamiento.length !== 0) {
          this.setState({ lista_tratamiento: episodio.lista_tratamiento })
        }
        if (episodio.lista_analitica.length !== 0) {
          this.setState({ lista_analitica: episodio.lista_analitica })
        }
        if (episodio.lista_procedimiento.length !== 0) {
          this.setState({ lista_procedimiento: episodio.lista_procedimiento })
        }
      })
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col-md-10">
            <h3>Tratamientos asociados</h3>
          </div>
          <div className="col-md-2">
            <Link className="btn btn-secondary" to={{ pathname: `/tratamiento/new/${this.props.match.params.paciente}/${this.props.match.params.id}` }}><i className="fas fa-plus"></i> Nuevo</Link>
          </div>
        </div>
        <br />
        {this.state.lista_tratamiento ? (<>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Id</th>
                <th>Inicio</th>
                <th>Fin</th>
                <th>Cuidados</th>
                <th>Importe</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              {this.state.lista_tratamiento.map(tratamiento => {
                return (
                  <tr key={tratamiento.id}>
                    <td>{tratamiento.id}</td>
                    <td>{new Date(tratamiento.fecha_inicio).toLocaleDateString()}</td>
                    <td>{new Date(tratamiento.fecha_fin).toLocaleDateString()}</td>
                    <td>{tratamiento.cuidados}</td>
                    <td>{tratamiento.importe}</td>
                    <td className="text-center">
                      <div className="btn-group">
                        <Link className="btn btn-secondary" to={{ pathname: `/tratamiento/view/${this.props.match.params.paciente}/${this.props.match.params.id}/${tratamiento.id}` }}><i className="far fa-eye"></i></Link>
                        <Link className="btn btn-secondary" to={{ pathname: `/tratamiento/update/${this.props.match.params.paciente}/${this.props.match.params.id}/${tratamiento.id}` }}><i className="far fa-edit"></i></Link>
                        <RemoveModal contenido={<i className="far fa-trash-alt"></i>} idBorrable={tratamiento.id} id={this.props.match.params.id} tabla="tratamiento" actualizar={this.actualizar} />
                      </div>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </>) : (<div className="row">No hay tratamientos asociados a este episodio</div>)}
        <br />
        <br />
        <div className="row">
          <div className="col-md-10">
            <h3>Analiticas asociadas</h3>
          </div>
          <div className="col-md-2">
            <Link className="btn btn-secondary" to={{ pathname: `/analitica/new/${this.props.match.params.paciente}/${this.props.match.params.id}` }}><i className="fas fa-plus"></i> Nuevo</Link>
          </div>
        </div>
        <br />
        {this.state.lista_analitica ? (<>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Id</th>
                <th>Prevista</th>
                <th>Realizacion</th>
                <th>Descripcion</th>
                <th>Ubicacion</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              {this.state.lista_analitica.map(analitica => {
                return (
                  <tr key={analitica.id}>
                    <td>{analitica.id}</td>
                    <td>{new Date(analitica.fecha_prevista).toLocaleDateString()}</td>
                    <td>{new Date(analitica.fecha_realizacion).toLocaleDateString()}</td>
                    <td>{analitica.descripcion}</td>
                    <td>{analitica.ubicacion}</td>
                    <td className="text-center">
                      <div className="btn-group">
                        <Link className="btn btn-secondary" to={{ pathname: `/analitica/view/${this.props.match.params.paciente}/${this.props.match.params.id}/${analitica.id}` }}><i className="far fa-eye"></i></Link>
                        <Link className="btn btn-secondary" to={{ pathname: `/analitica/update/${this.props.match.params.paciente}/${this.props.match.params.id}/${analitica.id}` }}><i className="far fa-edit"></i></Link>
                        <RemoveModal contenido={<i className="far fa-trash-alt"></i>} idBorrable={analitica.id} id={this.props.match.params.id} tabla="analitica" actualizar={this.actualizar} />
                      </div>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </>) : (<div className="row">No hay analiticas asociadas a este episodio</div>)}
        <br />
        <br />
        <div className="row">
          <div className="col-md-10">
            <h3>Procedimientos asociadas</h3>
          </div>
          <div className="col-md-2">
            <Link className="btn btn-secondary" to={{ pathname: `/procedimiento/new/${this.props.match.params.paciente}/${this.props.match.params.id}` }}><i className="fas fa-plus"></i> Nuevo</Link>
          </div>
        </div>
        <br />
        {this.state.lista_procedimiento ? (<>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Id</th>
                <th>Inicio</th>
                <th>Fin</th>
                <th>Informe</th>
                <th>Diagnostico inicial</th>
                <th>Diagnostico final</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              {this.state.lista_procedimiento.map(procedimiento => {
                return (
                  <tr key={procedimiento.id}>
                    <td>{procedimiento.id}</td>
                    <td>{new Date(procedimiento.fecha_inicio).toLocaleDateString()}</td>
                    <td>{new Date(procedimiento.fecha_fin).toLocaleDateString()}</td>
                    <td>{procedimiento.informe}</td>
                    <td>{procedimiento.diagnostico_inicial}</td>
                    <td>{procedimiento.diagnostico_final}</td>
                    <td className="text-center">
                      <div className="btn-group">
                        <Link className="btn btn-secondary" to={{ pathname: `/procedimiento/view/${this.props.match.params.paciente}/${this.props.match.params.id}/${procedimiento.id}` }}><i className="far fa-eye"></i></Link>
                        <Link className="btn btn-secondary" to={{ pathname: `/procedimiento/update/${this.props.match.params.paciente}/${this.props.match.params.id}/${procedimiento.id}` }}><i className="far fa-edit"></i></Link>
                        <RemoveModal contenido={<i className="far fa-trash-alt"></i>} idBorrable={procedimiento.id} id={this.props.match.params.id} tabla="procedimiento" actualizar={this.actualizar} />
                      </div>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </>) : (<div className="row">No hay Procedimientos asociados a este episodio</div>)}
      </div>
    );

  }
}

export default episodio;