import React, { useState, useEffect } from "react";
import { Table, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Procedimiento(props) {
  const [id, setId] = useState("")
  const [fecha_inicio, setFechaInicio] = useState("");
  const [fecha_fin, setFechaFin] = useState("");
  const [id_instrumentalista, setIdInstrumentalista] = useState("");
  const [id_episodio, setIdEpisodio] = useState(props.episodio);
  const [id_medico, setIdMedico] = useState("");
  const [id_dependencia, setIdDependencia] = useState("");
  const [informe, setInforme] = useState("");
  const [diagnostico_inicial, setDiagnosticoInicial] = useState("");
  const [diagnostico_final, setDiagnosticoFinal] = useState("");
  const [procedimiento_previsto, setProcedimientoPrevisto] = useState("");
  const [procedimiento_realizado, setProcedimientoRealizado] = useState("");
  const [id_prioridad, setIdPrioridad] = useState("");
  const [id_procedimiento, setIdProcedimiento] = useState("");
  const [loading, setLoading] = useState(true);
//  const validTel = RegExp(/^\d{9}$/i);


useEffect(() => {
  if(loading){
  props.setPaginaApp(<><Link to="/paciente/1/10">Pacientes</Link> > 
  <Link to={`/episodio/${props.match.params.paciente}/1/10`}>Ep. del paciente {props.match.params.paciente}</Link> > 
  <Link to={`/episodio/${props.match.params.paciente}/${props.match.params.episodio}`}>Episodio {props.match.params.episodio}</Link> > 
  <Navbar.Brand>Procedimiento {props.match.params.procedimiento}</Navbar.Brand></>)

async function onLoad() {
  fetch(`http://localhost:8082/procedimiento/get/${props.match.params.procedimiento}`, {
    method: 'GET',
    credentials: "include",
  }).then(response => {
    return response.json();
  }).then(procedimiento => {
    setId(procedimiento.id)
    setFechaInicio(procedimiento.fecha_inicio)
    setFechaFin(procedimiento.fecha_fin)
    setIdInstrumentalista(procedimiento.id_instrumentalista)
    setIdEpisodio(procedimiento.id_episodio)
    setIdMedico(procedimiento.id_medico)
    setIdDependencia(procedimiento.id_dependencia)
    setInforme(procedimiento.informe)
    setDiagnosticoInicial(procedimiento.diagnostico_inicial)
    setDiagnosticoFinal(procedimiento.diagnostico_final)
    setProcedimientoPrevisto(procedimiento.procedimiento_previsto)
    setProcedimientoRealizado(procedimiento.procedimiento_realizado)
    setIdPrioridad(procedimiento.id_prioridad)
    setIdProcedimiento(procedimiento.id_procedimiento)
  }).catch(err => err);
}
onLoad();
setLoading(false)
  }
}, [loading, props]);

  return (
    <div>
    <Table striped bordered hover>
      <tbody>
        <tr>
          <th>Id</th>
          <td>{id}</td>
        </tr>
        <tr>
          <th>Fecha Inicio</th>
          <td>{new Date(fecha_inicio).toLocaleDateString()}</td>
        </tr>
        <tr>
          <th>Fecha Fin</th>
          <td>{new Date(fecha_fin).toLocaleDateString()}</td>
        </tr>
        <tr>
          <th>Instrumentalista</th>
          <td>{id_instrumentalista ? (<>{id_instrumentalista.descripcion}</>):(<>No hay nada asignado a este campo</>)}</td>
        </tr>
        <tr>
          <th>Episodio</th>
          <td>{id_episodio}</td>
        </tr>
        <tr>
          <th>Medico</th>
          <td>{id_medico ? (<>{id_medico.descripcion}</>):(<>No hay nada asignado a este campo</>)}</td>
        </tr>
        <tr>
          <th>Dependencia</th>
          <td>{id_dependencia ? (<>{id_dependencia.descripcion}</>):(<>No hay nada asignado a este campo</>)}</td>
        </tr>
        <tr>
          <th>Informe</th>
          <td>{informe}</td>
        </tr>
        <tr>
          <th>Diagnostico Inicial</th>
          <td>{diagnostico_inicial}</td>
        </tr>
        <tr>
          <th>Diagnostico Final</th>
          <td>{diagnostico_final}</td>
        </tr>
        <tr>
          <th>Procedimiento Previsto</th>
          <td>{procedimiento_previsto}</td>
        </tr>
        <tr>
          <th>Procedimiento Realizado</th>
          <td>{procedimiento_realizado}</td>
        </tr>
        <tr>
          <th>Prioridad</th>
          <td>{id_prioridad ? (<>{id_prioridad.descripcion}</>):(<>No hay nada asignado a este campo</>)}</td>
        </tr>
        <tr>
          <th>Procedimiento</th>
          <td>{id_procedimiento ? (<>{id_procedimiento.descripcion}</>):(<>No hay nada asignado a este campo</>)}</td>
        </tr>
      </tbody>
    </Table>
  </div>
      );
}